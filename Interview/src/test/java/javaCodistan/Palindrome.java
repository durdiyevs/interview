package javaCodistan;

public class Palindrome {
	public static void main(String[] args) {
		// palindrome:a word, phrase, or sequence that reads the same backward as forward, e.g., madam or nurses run.

		System.out.println(isPalindrome("madam"));
	}

	public static boolean isPalindrome(String str) {
		if (str == null)
			return false;
		StringBuilder sb = new StringBuilder(str);
		return sb.reverse().toString().equals(str);
	}
}
