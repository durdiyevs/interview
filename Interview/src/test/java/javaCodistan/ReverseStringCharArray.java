package javaCodistan;

public class ReverseStringCharArray {
	public static void main(String[] args) {

		char[] JavaCharArray = {'r', 's', 't', 'u', 'v'};
		printReverse(JavaCharArray);
	}

	public static void printReverse(char[] letters) {
		for (int i = letters.length - 1; i >= 0; i--) {
			System.out.print(letters[i]);
		}
	}
}
