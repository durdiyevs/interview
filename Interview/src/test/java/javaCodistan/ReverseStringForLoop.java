package javaCodistan;

public class ReverseStringForLoop {

	public static void main(String[] args) {
		
		//BEST WAY:
		String name= "Interview study pdf";   
		String reversed = new StringBuilder(name).reverse().toString(); 
		System.out.println(reversed);
		
		
		
		//
		System.out.println(reverseString("Great video need to see more like this. keep up the good work."));
	}

	public static String reverseString(String str) {

		String reverse = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			reverse += str.charAt(i);

		}
		return reverse;
	}
	
	
	


}
