package javaOther;

public class ReverseStringWordBWord {

	public static void main(String[] args) {
		//Write a java program to reverse String? Reverse a string word by word?
		
		//Reverse String:
		//	Using Reverse Function: //show only after learning StringBuffer
			String a= "Hello Syntax";
			StringBuffer sb=new StringBuffer(a);
			System.out.println(sb.reverse());
		//	Without Using Reverse Function:
			String toReverse="Hello World";
			// 1 way using charAt();
			String reversed="";

			for (int i=toReverse.length()-1; i>=0; i--) {
			reversed=reversed+toReverse.charAt(i);
			}
			System.out.println("Reversed String is: "+reversed);
			//2 way using to charArray();
			String reversed1="";
			char[] array=toReverse.toCharArray();
			for (int i=array.length-1; i>=0; i--) {
			reversed1=reversed1+array[i];
			}
			System.out.println("Reversed String is: "+reversed1);
			//3 way using substring();
			String reversed2="";
			for (int i=toReverse.length(); i>=1; i--) {
			reversed2=reversed2+toReverse.substring(i-1, i);
			}
			System.out.println("Reversed String is: "+reversed2);

			//Reverse a string word by word
			String str = "Todays is Sunday";
			String reversed3 = "";
			String[] array3 = str.split(" ");
			for (int i = array.length - 1; i >= 0; i--) {
			reversed = reversed + array[i]+" ";
			}
			System.out.println(reversed);
	}

}
