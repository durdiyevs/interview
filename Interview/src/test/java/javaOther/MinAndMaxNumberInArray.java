package javaOther;

import java.util.Arrays;

public class MinAndMaxNumberInArray {

	public static void main(String[] args) {
		//1 easiest way
		int[] numArray= {12,13,12,15,0, -1};
		Arrays.sort(numArray);
		int min=numArray[0];
		int max=numArray[numArray.length-1];
		System.out.println("Min is="+min+" and max="+max);

		//2 more efficient way
		int[] numArray1= {12,13,12,15,0, -1};
		int smallest=0;
		int biggest=0;
		for(int i=0; i<numArray1.length; i++) {
		if (numArray1[i]>biggest){
		biggest=numArray1[i];
		}else {
		smallest=numArray1[i];
		}
		}
		System.out.println(smallest);

		System.out.println(biggest);
		}

	}


