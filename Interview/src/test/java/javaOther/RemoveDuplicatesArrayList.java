package javaOther;

import java.util.ArrayList;
import java.util.HashSet;

public class RemoveDuplicatesArrayList {

	public static void main(String[] args) {
		
		ArrayList aList=new ArrayList();
		ArrayList arr = new ArrayList<>();
		arr.add("John");
		arr.add("Jane");

		arr.add("James");
		arr.add("Jasmine");
		arr.add("Jane");
		arr.add("James");
		// 1 way
		HashSet set=new HashSet(aList);
		// 2 way
		HashSet hset=new HashSet();
		for (Object name : arr) {
		hset.add(name);
		}
		System.out.println(hset);

	}

}
